## SpringBoot和React教程
中级内容，适合有编程知识的朋友


### SpringBoot3

- 视频  
https://www.bilibili.com/video/BV1Es4y1q7Bf
- 笔记  
https://www.yuque.com/leifengyang/springboot3

日本IT技术要求很低，基本要求CRUD  
你只需要会写接收请求、处理SQL、返回响应就够了  

- 需要看的章节
  - springBoot3核心特性
  - 场景整合的web安全

- 不太重要的内容
  - Thymeleaf模板  
  基本都是前后端分离，使用React作为前端，不会用到Thymeleaf

- 不需要的内容：
  - 响应式编程  
  基本不会用到
  - 消息服务/远程调用等  
  对于日本IT过于高级，使用场景不多


### React
日本需要全栈的岗位挺多的，有时间的朋友建议学习一些前端的知识。  
你的CSS不需要很出色，但是框架和js要有一定水平。  
视频  
https://www.bilibili.com/video/BV1wy4y1D7JT


### 项目实战
看视频觉得很容易，写代码才能发现很多问题。  
你可以空想一个项目，自己编写。界面不需要美观，但是功能要充实。  
举个例子：  
请你写一个交友网站  
请包含以下功能
  - 用户注册/登录
  - 用户上传头像
  - 用户填写个人资料
  - 用户给其他人点赞
  - 用户查看给自己点赞的人
  - 允许用户从网站下载文件

请实现以下的技术要求
  - 前后端分离


